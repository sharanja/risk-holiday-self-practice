package bcas.pro.cal;
/*
 * To perform basic calculator functions
 * 
 * @author Sharanja
 * @since 20-05-2020
 * @version 1.0.0v
 */
public class SimpleCalculator {

	/*
	 * variable a, number 1 value for the addition
	 */
		public int a;
		
	/*
	 * variable b, number 2 value for the addition
	 */
		public int b;
		
	/*
	 * to add two integer numbers.
	 * will return sum of given two integers (a+b)
	 * 
	 * @param a addition first value
	 * @param b addition second value
	 * @return (a+b)
	 * 
	 */
		
		public int sum(int a,int b) {
			return a + b;
			
		}
		
	/*
	 * to subtract two integer numbers.
	 * will return sum of given two integers (a-b / b-a)
	 * 
	 * @param a subtraction first value
	 * @param b subtraction second value
	 * @return (a-b)
	 * 
	 */
		
		public int sub(int a, int b) {
			return a - b;
		}
		
	/*
	 * This is the main method of this program
	 * 	@param args
	 */
		
		public static void main(String[] args) {

				SimpleCalculator cal = new SimpleCalculator();
				System.out.println(cal.sub(9 ,  5));
				System.out.println(cal.sum(9 , 5));
	}

}
