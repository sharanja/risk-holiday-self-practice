package bcas.pro.str;

	/**
	 * To perform basic string methods
	 * 
	 * @author Sharanja
	 * @since 21.05.2020
	 * @version 1.0.0v
	 *
	 */

public class Str {
			
	/**
	* This is the main method of this program
	* @param args for input arguments
	*/
		
public static void main(String[] args) {
			
	/** 
	* String variable text, string of the word
	*/
			
			String text = "    Believe in yourself!    ";
		
	/**
	* print after trim 
	*/
			
			String trim = text.trim();
			System.out.println(trim);		
}	
}


/*
 * 
 * INPUT - "    Believe in yourself!    "
 * OUTPUT - "Believe in yourself"
 * 
 */
