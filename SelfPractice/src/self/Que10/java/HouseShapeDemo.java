package self.Que10.java;

public class HouseShapeDemo {
	public static void main(String[] args) {
		int rows = 6;

		HouseShape demo = new HouseShape();
	
		demo.roof(rows);
		demo.body(rows);
		demo.message();
	}
}

/*

OUTPUT
	

     /\
    /  \
   /    \
  / +--+ \
 /  |  |  \
/   +--+   \
-+--------+-
 |        |
 |  +--+  |
 |  |  |  |
 |  +--+  |
 |        |
-+--------+-
This is my house !
							*/
