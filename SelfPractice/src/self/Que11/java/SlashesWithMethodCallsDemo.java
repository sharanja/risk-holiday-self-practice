package self.Que11.java;
public class SlashesWithMethodCallsDemo {
	public static void main(String[] args) {
		int i = 0;
		int a = 0;
		SlashesWithMethodCalls demo = new SlashesWithMethodCalls();
		
		demo.twoLines();
		demo.twoLines();
		demo.twoLines();
		demo.twoLines();
		demo.twoLines();
		
	}
}

/*

OUTPUT
/ / / / / / / / / / / / 
/ / / / / / / / / / /
/ / / / / / / / / / / / 
/ / / / / / / / / / /
/ / / / / / / / / / / / 
/ / / / / / / / / / /
/ / / / / / / / / / / / 
/ / / / / / / / / / /
/ / / / / / / / / / / / 
/ / / / / / / / / / /
						*/