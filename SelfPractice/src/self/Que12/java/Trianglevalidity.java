package self.Que12.java;
import java.util.Scanner;

public class Trianglevalidity {
	public static void main(String[] args){
		
	  Scanner in = new Scanner(System.in);
	  System.out.print("Input sideA : ");
	  double A = in .nextDouble();
	  System.out.print("Input sideB : ");
	  double B = in .nextDouble();
	  System.out.print("Input sideC : ");
	  double C = in .nextDouble();

	  	System.out.print("Is valid Triangle: " + isValidTriangle(A, B, C));
	}
	
	public static boolean isValidTriangle(double A, double B, double C){
		return (A + B > C && B + C > A && C + A > B);
	}
}




/*

OUTPUT

Input sideA : 5
Input sideB : 4
Input sideC : 3
Is valid Triangle: true

	or
	
	 
Input sideA : 5
Input sideB : 6
Input sideC : 15
Is valid Triangle: false

						  */