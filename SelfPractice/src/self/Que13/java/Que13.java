package self.Que13.java;
import java.util.Scanner;

public class Que13 {
	public static void main(String[] args){
		
		  Scanner in = new Scanner(System.in);
		  System.out.print("Input sideA : ");
		  double A = in .nextDouble();
		  System.out.print("Input sideB : ");
		  double B = in .nextDouble();
		  System.out.print("Input sideC : ");
		  double C = in .nextDouble();

		  	System.out.print("Is Right Angle Triangle: " + isRightAngleTriangle(A, B, C));
		}

	   public static boolean isRightAngleTriangle(double A, double B, double C) {
	        if((Math.pow(A,2)+Math.pow(B,2)) == Math.pow(C,2))
	            return true;
	        if((Math.pow(B,2)+Math.pow(C,2)) == Math.pow(B,2))
	            return true;
	        if((Math.pow(C,2)+Math.pow(B,2)) == Math.pow(A,2))
	            return true;
	        else
	            return false;
	    }
}

/*

OUTPUT 

Input sideA : 3
Input sideB : 4
Input sideC : 5
Is Right Angle Triangle: true


		OR


Input sideA : 2
Input sideB : 2
Input sideC : 2
Is Right Angle Triangle: false
									
								*/
