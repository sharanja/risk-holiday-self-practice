package self.Que14.java;
import java.util.Scanner;

public class Que14 {
		public static void main(String[] args){
			
			  Scanner in = new Scanner(System.in);
			  System.out.print("Input valueA : ");
			  double A = in .nextDouble();
			  System.out.print("Input valueB : ");
			  double B = in .nextDouble();
			  System.out.print("Input valueC : ");
			  double C = in .nextDouble();

			  	System.out.print("Are the values all positive : " + allPositive(A, B, C));
			}

		   public static boolean allPositive(double A, double B, double C) {
			   return(A>=0 && B>=0 && C>=0);
	   
		    }
	}


/* 

OUTPUT

Input valueA : 2
Input valueB : 3
Input valueC : 2
Are the values all positive : true

		OR

Input valueA : -2
Input valueB : 3
Input valueC : 5
Are the values all positive : false

										 */