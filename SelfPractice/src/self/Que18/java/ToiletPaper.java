package self.Que18.java;
import java.util.Scanner;

public class ToiletPaper {
	public static void main(String[] args) {

	        Scanner in = new Scanner(System.in);
	        System.out.print("Enter the Core Radius of the Toilet Paper : ");
	        double coreRadius = in.nextDouble();
	        
	        Scanner in1 = new Scanner(System.in);
	        System.out.print("Enter the Max Radius of the Toilet Paper : ");
	        double maxRadius = in1.nextDouble();
	        
	        Scanner in2 = new Scanner(System.in);
	        System.out.print("Enter the Thickness of the Toilet Paper : ");
	        double thickness = in2.nextDouble();
	        
	        double pi =22/7;
	        double radius=coreRadius;
	        double totalLengthofToiletPaper = 0;
	        
	        for(int rounds=1; rounds>=1; rounds++){
	        	
	        	radius = radius + rounds*thickness;
	        	totalLengthofToiletPaper = totalLengthofToiletPaper + 2 * pi * radius;
	        	
	        		if (radius >= maxRadius){
	        			break;
	        		}
	        	
	        }
	        
	        totalLengthofToiletPaper=(int)(Math.floor(totalLengthofToiletPaper));
	        System.out.println("The Length of the Toilet paper is : "+totalLengthofToiletPaper);
	} 	
}



/*
 
 OUTPUT 
 
Enter the Core Radius of the Toilet Paper : 7
Enter the Max Radius of the Toilet Paper : 70
Enter the Thickness of the Toilet Paper : 1
The Length of the Toilet paper is : 2178.0

 												 */