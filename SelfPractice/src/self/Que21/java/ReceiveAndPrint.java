package self.Que21.java;
import java.util.Scanner;

public class ReceiveAndPrint{
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the string value : ");
		String s = scan.nextLine();
		
		System.out.println("Enter the int value : ");
		int m = scan.nextInt();
		
		System.out.println("Enter the double value : ");
		double d = scan.nextDouble();
		
			System.out.println("s : "+ s);
			System.out.println("m : "+ m);
			System.out.println("d : "+ d);
			System.out.println("m + d + s : "+(m + d + s));
			System.out.println("m + s + d : "+(m + s + d));
			System.out.println("s + m + d : "+(s + m + d));
	}
}


/*

OUTPUT


Enter the string value : 
java
Enter the int value : 
4
Enter the double value : 
4.20
s : java
m : 4
d : 4.2
m + d + s : 8.2java
m + s + d : 4java4.2
s + m + d : java44.2
	
								*/