package self.Que23.java;

public class StringMethods {
	public static void main(String[] args) {
		String word = "School.of.Progressive. Rock";
		String pat = "in";
	
			System.out.println("A. word.length() \t \t: "+word.length());
			System.out.println("B. word.substring(22) \t \t: "+word.substring(22));
			System.out.println("C. word.substring(22, 24) \t: "+word.substring(22, 24));
			System.out.println("D. word.indexOf(\"oo\") \t \t: "+word.indexOf("oo"));
			System.out.println("E. word.toUpperCase() \t \t: "+word.toUpperCase());
			System.out.println("F. word.lastIndexOf(\"o\") \t: "+word.lastIndexOf("o"));
			System.out.println("G. word.indexOf(\"ok\") \t \t: "+word.indexOf("ok"));

	}
}

/*

OUTPUT

A. word.length() 	 		: 27
B. word.substring(22) 	 	:  Rock
C. word.substring(22, 24) 	:  R
D. word.indexOf("oo") 	 	: 3
E. word.toUpperCase() 	 	: SCHOOL.OF.PROGRESSIVE. ROCK
F. word.lastIndexOf("o") 	: 24
G. word.indexOf("ok") 	 	: -1

															  */