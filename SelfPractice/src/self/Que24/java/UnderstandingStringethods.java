package self.Que24.java;

public class UnderstandingStringethods {
	public static void main(String[] args) {
		String w = "Singin�_in_the_rain";
		String pat = "in";
	
			System.out.println("1) w.indexOf(pat) \t: "+w.indexOf(pat));
			System.out.println("2) w.indexOf(pat,3) \t: "+w.indexOf(pat,3));
			System.out.println("3) w.indexOf(pat,6) \t: "+w.indexOf(pat,6));
			System.out.println("4) w.lastIndexOf(pat) \t: "+w.lastIndexOf(pat));
			System.out.println("5) w.length() \t \t: "+w.length());
			System.out.println("6) w.toUpperCase() \t: "+w.toUpperCase());
			System.out.println("7) w.charAt(0) \t \t: "+w.charAt(0));

	}
}


/*

OUTPUT 


1) w.indexOf(pat) 		: 1
2) w.indexOf(pat,3) 	: 4
3) w.indexOf(pat,6) 	: 8
4) w.lastIndexOf(pat) 	: 17
5) w.length() 	 		: 19
6) w.toUpperCase() 		: SINGIN�_IN_THE_RAIN
7) w.charAt(0) 	 		: S
 										*/