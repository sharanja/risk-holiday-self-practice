package self.Que26.java;
import java.util.Scanner;

public class SuffixesOfAVariable {
	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		System.out.print("Enter the word :");
		String word = in.nextLine();
				
		int length = word.length();
		
		for (int row = 0; row < length + 1; row++) {
			for (int i = row; i < row + 1; i++) {
				System.out.print(word.substring(i));
			}
				System.out.println();
		}
	}	
}


/*

OUTPUT 

Enter the word :hurricanes


hurricanes
urricanes
rricanes
ricanes
icanes
canes
anes
nes
es
s

					*/