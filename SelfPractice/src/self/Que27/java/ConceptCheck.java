package self.Que27.java;

public class ConceptCheck {
	public static void main(String[] args) {
		String s = "Mississippi";
	
			System.out.println("a) s.length() \t \t \t \t: "+s.length());
			System.out.println("b) s.indexOf( \"si\" ) \t \t \t: "+s.indexOf( "si" ));
			System.out.println("c) s.toUpperCase().indexOf( \"si\" ) \t: "+s.toUpperCase().indexOf( "si" ));
			System.out.println("d) s.toLowerCase().indexOf( \"si\" ) \t: "+s.toLowerCase().indexOf( "si" ));
			System.out.println("e) s.substring(0,s.indexOf( \"i\" )) \t: "+s.substring(0,s.indexOf( "i" )));
			System.out.println("f) s.substring( s.lastIndexOf( \"i\" ) ) \t: "+s.substring( s.lastIndexOf( "i" ) ));

	}
}

/* 

OUTPUT 

a) s.length() 	 	 	 				: 11
b) s.indexOf( "si" ) 	 	 			: 3
c) s.toUpperCase().indexOf( "si" ) 		: -1
d) s.toLowerCase().indexOf( "si" ) 		: 3
e) s.substring(0,s.indexOf( "i" )) 		: M
f) s.substring( s.lastIndexOf( "i" ) ) 	: i
													
													*/