package self.Que28.java;
import java.util.Scanner;

public class StringReverse {
	 public static void main(String args[]){
		   
		   Scanner in = new Scanner(System.in);
		   System.out.print("Enter an input String : ");
		   String original = in.nextLine();
		    
		   String reverse = "";
		   int length = original.length();

		    for (int i = length - 1 ; i >= 0 ; i--) {
		    	reverse = reverse + original.charAt(i);
		    }
		    	System.out.println("The reverse of "+ original +" is " + reverse);
	}
}

/*

OUTPUT 

Enter an input String : Computer-Programming
The reverse of Computer-Programming is gnimmargorP-retupmoC

																*/