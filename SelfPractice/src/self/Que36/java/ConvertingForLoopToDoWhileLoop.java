package self.Que36.java;

public class ConvertingForLoopToDoWhileLoop {
	public static void main(String[] args) {
		int index = 1;
		do {			
				System.out.println(index);
				index = index + 3 ;
		}
		
		while (index <= 15);
	}
}



/*

OUTPUT

1
4
7
10
13
		 */