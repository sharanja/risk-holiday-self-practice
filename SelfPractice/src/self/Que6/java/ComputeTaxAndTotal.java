package self.Que6.java;
import java.util.Scanner;

public class ComputeTaxAndTotal {
		public static void main(String[] args) {
			Scanner in = new Scanner (System.in);
			System.out.print("Enter the subtotal :");
			int subtotal = in.nextInt();
			
			Scanner scan = new Scanner (System.in);
			System.out.print("Enter the tax rate :");
			double taxPercent = scan.nextDouble();
			int tax = (int) ((subtotal * taxPercent)/100);
			int total = subtotal + tax;
			
			System.out.println("The subtotal = "+subtotal/100+" dollars and "+subtotal%100+" cents.");
			System.out.println("The tax rate = "+ taxPercent +" percent.");
			System.out.println("The tax = "+tax/100+" dollars and "+tax%100+" cents.");
			System.out.println("The total = "+ total/100+" dollars and "+ total%100+" cents.");
		}
	}


	/*
	 
	OUTPUT
	
The subtotal = 110 dollars and 50 cents.
The tax rate = 5.5 percent.
The tax = 6 dollars and 7 cents.
The total = 116 dollars and 57 cents.


	 										*/