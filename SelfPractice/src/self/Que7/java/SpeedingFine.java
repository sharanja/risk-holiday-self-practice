package self.Que7.java;
import java.util.Scanner;

public class SpeedingFine {
		public static void main(String[] args) {				
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter combination number = ");
			int combination =scan.nextInt();
			
			switch (combination){
					
				case 1 : 
					if (combination == 1) {
						int speed = 50;
						int limit = 35;
						int fine = (speed - limit) * 20;
					
						System.out.println("The fine for driving at " + speed + " mph on a 35 mph road is "+ fine + " dollars.");	
					}
						
				case 2 : 
					if (combination == 2) {
						int speed = 30;
						int limit = 25;
						int fine = (speed - limit) * 20;
						
						System.out.println("The fine for driving at " + speed + " mph on a 25 mph road is "+ fine + " dollars.");
					}
					
				case 3:
					if (combination == 3) {
						int speed = 60;
						int limit = 45;
						int fine = (speed - limit) * 20;
					
						System.out.println("The fine for driving at " + speed + " mph on a 45 mph road is "+ fine + " dollars.");
					}			
			}				
		}
	}			


	/*
	 
	INPUT : 1
	OUTPUT : The fine for driving at 50 mph on a 35 mph road is 300 dollars.


	INPUT : 2
	OUTPUT : The fine for driving at 30 mph on a 25 mph road is 100 dollars.

	INPUT : 3
	OUTPUT : The fine for driving at 60 mph on a 45 mph road is 300 dollars.
	 																		   */
