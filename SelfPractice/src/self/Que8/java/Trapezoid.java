package self.Que8.java;
import java.util.Scanner;

public class Trapezoid {
		public static void main(String[] args) {
			Scanner scan = new Scanner (System.in);
			System.out.print("Enter the top :");
			double top= scan.nextDouble();
			
			System.out.print("Enter the bottom :");
			double bottom= scan.nextDouble();
			
			System.out.print("Enter the height :");
			double height= scan.nextDouble();
			
			System.out.print("Top : ");
			System.out.println(top);
			System.out.print("Bottom : ");
			System.out.println(bottom);
			System.out.print("Height : ");
			System.out.println(height);
			System.out.print("Area : ");
			System.out.println((bottom + top) * height / 2);
			
		}
	}


	/*

	OUTPUT


	Top : 10.0
	Bottom : 20.5
	Height : 24.4
	Area : 372.09999999999997

	 							*/