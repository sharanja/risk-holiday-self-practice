package self.Que9.java;
import java.util.Scanner;

public class PlayWithNumbersDecomposed {
			public void numberManipulation1(int a , int b){
				Scanner scan = new Scanner(System.in);
				System.out.println("Enter two integers:  ");
				a =scan.nextInt();
				b =scan.nextInt();
				
					System.out.println("a + b is equal to "+ (a+b));
					System.out.println("a - b is equal to "+(a-b));		
					System.out.println("a * b is equal to "+ (a*b));
					System.out.println("a / b is equal to "+ (a/b));
					System.out.println("a % b is equal to "+ (a%b));
			}
					
			public void numberManipulation2(int a , int b ,int c){
				Scanner scan = new Scanner(System.in);
				System.out.println("Enter three integers:  ");
				a =scan.nextInt();
				b =scan.nextInt();
				c =scan.nextInt();
					
					System.out.println("(a - b)/c is equal to "+ (a - b)/c);
					System.out.println("(a - c)/b is equal to "+ (a - c)/b);		
					System.out.println("(b - c)/a is equal to "+ (b - c)/a);
					System.out.println("(b - a)/c is equal to "+ (b - a)/c);
					System.out.println("(c - a)/b is equal to "+ (c - a)/b);	
					System.out.println("(c - b)/a is equal to "+ (c - b)/a);
			}
	}  