package self.Que9.java;

public class PlayWithNumbersDecomposedDemo {
		public static void main(String[] args) {
			int a = 0;
			int b = 0;
			int c = 0;
			PlayWithNumbersDecomposed demo = new PlayWithNumbersDecomposed();
		
			demo.numberManipulation1(a,b);
			demo.numberManipulation2(a,b,c);
		}
	}


	/*
	OUTPUT

	Enter two integers:  
	100435
	345

	a + b is equal to 100780
	a - b is equal to 100090
	a * b is equal to 34650075
	a / b is equal to 291
	a % b is equal to 40




	Enter three integers:  
	34325
	79
	-40

	(a - b)/c is equal to -856
	(a - c)/b is equal to 435
	(b - c)/a is equal to 0
	(b - a)/c is equal to 856
	(c - a)/b is equal to -435
	(c - b)/a is equal to 0


	 									*/