public class FindArea {

	public double triangle(double base, double height) {
		double area = 0.5 * base * height;
		return area;
	}
	
	public double reactangle(double width, double height) {
		return width * height;
	}
	
	public double squre(double length) {
		return Math.pow(length, 2);
	}
	
	public double circle(double radious) {
		double area = Math.PI * Math.pow(radious, 2);
		return area;
	}
	
	public String saySharanja() {
		return "Sharanja";
	}
}