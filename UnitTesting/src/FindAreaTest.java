import org.junit.Test;
import junit.framework.Assert;
public class FindAreaTest {

	FindArea findArea = new FindArea();
	
	@Test
	public void circle_Test(){
		
		Assert.assertEquals(153.93804002589985,findArea.circle(7));		
	}
	
	@Test
	public void circle_Test_Withzero(){

		Assert.assertEquals(0.0,findArea.circle(0));		
	}
	
	@Test
	public void reactangle_Test(){

		Assert.assertEquals(30.0,findArea.reactangle(6,5));
	}

	public void saySharanja_Test() {
		
		Assert.assertEquals("Hi",findArea.saySharanja());
	}
	
	@Test
	public void triangle_Test(){

		Assert.assertEquals(4.0,findArea.triangle(2,4));
	}

	@Test
	public void squre_Test(){

		Assert.assertEquals(25.0,findArea.squre(5));
	}

}
	
